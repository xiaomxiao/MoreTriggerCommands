package org.xm.moretriggercommands;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.scheduler.BukkitTask;
import org.xm.moretriggercommands.Event.*;

public class EventSwitch implements Listener {

    private BukkitTask tpsTask;

    private TPSUtil tpsUtil;

    @EventHandler
    public PluginManager pluginManager() {
        PluginManager pluginManager = Main.getInstance().getServer().getPluginManager();
        System.out.println("EventSwitch-PluginManager已加载");

        if (Boolean.parseBoolean(Main.getInstance().getConfig().getString("TPScommand.Switch"))) {
            tpsUtil = new TPSUtil(Main.getInstance()); // 初始化成员变量
        }
        if (Boolean.parseBoolean(Main.getInstance().getConfig().getString("KillNotifier.Switch"))) {
            Main.getInstance().getServer().getPluginManager().registerEvents(new KillNotifierPlugin(), Main.getInstance());
        }
        if (Boolean.parseBoolean(Main.getInstance().getConfig().getString("CustomizeCommand.Switch"))) {
            Main.getInstance().getServer().getPluginManager().registerEvents(new CustomizeCommand(), Main.getInstance());
        }
        if (Boolean.parseBoolean(Main.getInstance().getConfig().getString("LifeLessThan.Switch"))) {
            Main.getInstance().getServer().getPluginManager().registerEvents(new LifeLessThan(), Main.getInstance());
        }
        if (Boolean.parseBoolean(Main.getInstance().getConfig().getString("InventoryClose.Switch"))) {
            pluginManager.registerEvents(new InventoryClose(), Main.getInstance());
        }
        if (Boolean.parseBoolean(Main.getInstance().getConfig().getString("InventoryOpen.Switch"))) {
            pluginManager.registerEvents(new InventoryOpen(), Main.getInstance());
        }

        if (Boolean.parseBoolean(Main.getInstance().getConfig().getString("Join.Switch"))) {
            pluginManager.registerEvents(new Join(), Main.getInstance());
        }

        if (Boolean.parseBoolean(Main.getInstance().getConfig().getString("PlayerDeath.Switch"))) {
            pluginManager.registerEvents(new PlayerDeath(), Main.getInstance());
        }

        if (Boolean.parseBoolean(Main.getInstance().getConfig().getString("PlayerRespawn.Switch"))) {
            pluginManager.registerEvents(new PlayerRespawn(), Main.getInstance());
        }

        if (Boolean.parseBoolean(Main.getInstance().getConfig().getString("ResourcePackStatus.Switch"))) {
            pluginManager.registerEvents(new ResourcePackStatus(), Main.getInstance());
        }

        if (Boolean.parseBoolean(Main.getInstance().getConfig().getString("PlayerBedEnter.Switch"))) {
            pluginManager.registerEvents(new PlayerBedEnter(), Main.getInstance());
        }

        if (Boolean.parseBoolean(Main.getInstance().getConfig().getString("PlayerBedLeave.Switch"))) {
            pluginManager.registerEvents(new PlayerBedLeave(), Main.getInstance());
        }

        if (Boolean.parseBoolean(Main.getInstance().getConfig().getString("PlayerDropItem.Switch"))) {
            pluginManager.registerEvents(new PlayerDropItem(), Main.getInstance());
        }

        if (Boolean.parseBoolean(Main.getInstance().getConfig().getString("PlayerChangedWorld.Switch"))) {
            pluginManager.registerEvents(new PlayerChangedWorld(), Main.getInstance());
        }

        if (Boolean.parseBoolean(Main.getInstance().getConfig().getString("JoinRegion.Switch"))) {
            pluginManager.registerEvents(new RegionListener(), Main.getInstance());
        }
        return pluginManager;
    }
}
