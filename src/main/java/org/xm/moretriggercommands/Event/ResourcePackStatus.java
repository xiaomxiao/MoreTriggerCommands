package org.xm.moretriggercommands.Event;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerResourcePackStatusEvent;
import org.xm.moretriggercommands.Main;

public class ResourcePackStatus implements Listener {

    @EventHandler
    public void onResourcePackStatus(PlayerResourcePackStatusEvent event) {
        Player player = event.getPlayer();
        //#资源包成功地下载并应用到了客户端。
        if (event.getStatus() == PlayerResourcePackStatusEvent.Status.SUCCESSFULLY_LOADED) {
            for (String Command : Main.getInstance().getConfig().getStringList("ResourcePackStatus.SUCCESSFULLY_LOADEDCommands")) {
                Command = Command.replace("%player_name%", player.getName()).replace("&","§");
                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), Command);
            }
        }
        //#客户端接受了资源包, 但下载失败。
        if (event.getStatus() == PlayerResourcePackStatusEvent.Status.FAILED_DOWNLOAD) {
            for (String Command : Main.getInstance().getConfig().getStringList("ResourcePackStatus.FAILED_DOWNLOADCommands")) {
                Command = Command.replace("%player_name%", player.getName()).replace("&","§");
                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), Command);
            }
        }
        //#客户端拒绝接受资源包。
        if (event.getStatus() == PlayerResourcePackStatusEvent.Status.DECLINED) {
            for (String Command : Main.getInstance().getConfig().getStringList("ResourcePackStatus.DECLINEDCommands")) {
                Command = Command.replace("%player_name%", player.getName()).replace("&","§");
                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), Command);
            }
        }
        //#客户端接受了资源包, 并开始下载。
        if (event.getStatus() == PlayerResourcePackStatusEvent.Status.ACCEPTED) {
            for (String Command : Main.getInstance().getConfig().getStringList("ResourcePackStatus.ACCEPTEDCommands")) {
                Command = Command.replace("%player_name%", player.getName()).replace("&","§");
                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), Command);
            }
        }
    }
}