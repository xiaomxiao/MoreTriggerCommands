package org.xm.moretriggercommands.Event;

import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.xm.moretriggercommands.Main;

public class KillNotifierPlugin implements Listener {

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent event) {
        Player deadPlayer = event.getEntity();
        Entity killerEntity = event.getEntity().getKiller();
        if (killerEntity instanceof Player) {
            Player killerPlayer = (Player) killerEntity;
            for (String Command : Main.getInstance().getConfig().getStringList("KillNotifier.KillNotifierCommand")) {
                Command = Command
                        .replace("%Drop_name%", deadPlayer.getName())
                        .replace("%player_name%", killerPlayer.getName())
                        .replace("&","§");
                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), Command);
            }

        }

    }
}