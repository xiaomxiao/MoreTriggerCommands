package org.xm.moretriggercommands.Event;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.xm.moretriggercommands.Main;

import java.util.List;

public class CustomizeCommand implements Listener {

    @EventHandler
    public void onCustomizeCommand(AsyncPlayerChatEvent event) {

        Player player = event.getPlayer();
        String message = event.getMessage();
        List<String> List = Main.getInstance().getConfig().getStringList("CustomizeCommand.CCommand");
        for (String Listxm : List) {


            List<String> eventList_a = Main.getInstance().getConfig().getStringList(Listxm + ".Customize-Command");
            List<String> eventList_b = Main.getInstance().getConfig().getStringList(Listxm + ".Execute-Command");
            for (String aaa : eventList_a) {

                if (message.startsWith(aaa)) {
/*
                    int cd = Main.getInstance().getConfig().getInt(Listxm + ".CD");
                    String CdMessage = Main.getInstance().getConfig().getString(Listxm + ".CDMessage");

                   // 检查玩家是否处于冷却状态
                    if (Main.getInstance().cooldowns.containsKey(player.getName())) {
                        long secondsLeft = ((Main.getInstance().cooldowns.get(player.getName()) / 1000) + cd) - (System.currentTimeMillis() / 1000);
                        if (secondsLeft > 0) {
                            CdMessage = CdMessage.replace("%CD%", Long.toString(secondsLeft));
                            player.sendMessage(CdMessage);
                            return;
                        }
                    }
                    Main.getInstance().cooldowns.put(player.getName(), System.currentTimeMillis());
                    */

                    for (String command : eventList_b) {
//                    Main.getInstance().getLogger().info("测试注册定义的指令:" + Lista);
//                    Main.getInstance().getLogger().info("测试触发的指令:" + aaa);
//                    Main.getInstance().getLogger().info("测试执行的指令:" + command);
                        command = command.replace("%player_name%", player.getName()).replace("&", "§");
                        Main.getInstance().getLogger().info("玩家执行了" + command + "指令");
                        String Commanda = command;
                        Bukkit.getScheduler().runTask(Main.getInstance(), () -> {
                            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), Commanda);
                        });
                    }

                }
            }
        }
    }
}