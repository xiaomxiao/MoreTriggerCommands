package org.xm.moretriggercommands.Event;


import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.xm.moretriggercommands.Main;

public class Join implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        if (!player.hasPlayedBefore()) {      //判断玩家是否进入过服务器(检测世界文件的)
            for (String Command : Main.getInstance().getConfig().getStringList("Join.FirstCommand")) {
                Command = Command.replace("%player_name%", player.getName()).replace("&","§");
                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), Command);
            }
        } else {
            for (String EnterCommands : Main.getInstance().getConfig().getStringList("Join.EnterCommands")) {
                EnterCommands = EnterCommands.replace("%player_name%", player.getName()).replace("&","§");
                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), EnterCommands);
            }
        }

    }
}