package org.xm.moretriggercommands.Event;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.xm.moretriggercommands.Main;


public class LifeLessThan implements Listener {

    @EventHandler
    public void onLifeLessThan(EntityDamageEvent e) {
        if (e.getEntity() instanceof Player) {
            Player player = (Player) e.getEntity();
            double healthPercent = player.getHealth() / player.getMaxHealth() * 100;
            double lowHealthThreshold = Main.getInstance().getConfig().getDouble("LifeLessThan.LifeLessThanPercent");
            if (healthPercent < lowHealthThreshold) {
                for (String Command : Main.getInstance().getConfig().getStringList("LifeLessThan.LifeLessThanCommand")) {
                    Command = Command
                            .replace("%player_name%", player.getName())
                            .replace("&", "§");
                    Bukkit.dispatchCommand(Bukkit.getConsoleSender(), Command);
                }
            }
        }
    }

}