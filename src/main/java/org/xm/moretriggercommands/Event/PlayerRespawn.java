package org.xm.moretriggercommands.Event;


import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Statistic;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.xm.moretriggercommands.Main;

import java.text.SimpleDateFormat;
import java.util.Date;

public class PlayerRespawn implements Listener {

    @EventHandler
    public void noPlayerRespawn(PlayerRespawnEvent e) {
        String command_console_cd = Main.getInstance().getConfig().getString("PlayerRespawn.Console-Command-Cd");
        String player_console_cd = Main.getInstance().getConfig().getString("PlayerRespawn.Player-Command-Cd");
        Player player = e.getPlayer();
        for (String cmd : Main.getInstance().getConfig().getStringList("PlayerRespawn.Console-Commands"))
            //创建Bukkit调度管理器，并延迟
            Bukkit.getScheduler().runTaskLater(Main.getInstance(), new Runnable() {
                public void run() {
                    Bukkit.dispatchCommand(Main.getInstance().getServer().getConsoleSender(), translatePlaceholders(cmd, player, e));
                }
            }, Long.parseLong(command_console_cd));

        for (String cmd : Main.getInstance().getConfig().getStringList("PlayerRespawn.Player-Commands")) {
            //创建Bukkit调度管理器，并延迟
            Bukkit.getScheduler().runTaskLater(Main.getInstance(), new Runnable() {
                public void run() {
                    Bukkit.dispatchCommand( player, translatePlaceholders(cmd, player, e));
                }
            }, Long.parseLong(player_console_cd));
        }
    }
    private String translatePlaceholders(String cmd, Player player ,PlayerRespawnEvent e) {
        Location location = player.getLocation();
        int deathNumber = player.getStatistic(Statistic.DEATHS);
        Date day=new Date();
        String date = Main.getInstance().getConfig().getString("PlayerRespawn.Date");
        SimpleDateFormat df = new SimpleDateFormat(date);
        return cmd.replaceAll("%player_name%", player.getName())
                .replaceAll("%world%", location.getWorld().getName())
                .replaceAll("%DeathNumber%", Integer.toString(deathNumber))
                .replaceAll("%Deathdate%", df.format(day))
                .replaceAll("%RespawnLocation%", String.valueOf(e.getRespawnLocation()))
                .replaceAll("&", "§");
    }

}