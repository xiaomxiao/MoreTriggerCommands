package org.xm.moretriggercommands.Event;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerBedEnterEvent;
import org.xm.moretriggercommands.Main;

public class PlayerBedEnter implements Listener {

    @EventHandler
    public void onPlayerBedEnter(PlayerBedEnterEvent e){
        for (String Command : Main.getInstance().getConfig().getStringList("PlayerBedEnter.PlayerBedEnter")) {
            Player player = e.getPlayer();
            Command = Command.replace("%player_name%", player.getName()).replace("&","§");
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), Command);
        }
    }
}