package org.xm.moretriggercommands.Event;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerBedLeaveEvent;
import org.xm.moretriggercommands.Main;

public class PlayerBedLeave implements Listener {

    @EventHandler
    public void onPlayerBedLeave(PlayerBedLeaveEvent e){
        for (String Command : Main.getInstance().getConfig().getStringList("PlayerBedLeave.PlayerBedLeave")) {
            Player player = e.getPlayer();
            Command = Command.replace("%player_name%", player.getName()).replace("&","§");
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), Command);
        }
    }
}