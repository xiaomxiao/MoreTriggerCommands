package org.xm.moretriggercommands.Event;


import org.bukkit.Bukkit;
import org.bukkit.entity.HumanEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.xm.moretriggercommands.Main;

public class InventoryOpen implements Listener {

    @EventHandler
    public void onInventoryOpen(InventoryOpenEvent event) {
        HumanEntity player = event.getPlayer();
        for (String Command : Main.getInstance().getConfig().getStringList("InventoryOpen.InventoryOpen")) {
            Command = Command.replace("%player_name%", player.getName()).replace("&", "§");
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), Command);
        }
    }

}