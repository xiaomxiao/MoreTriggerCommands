package org.xm.moretriggercommands.Event;


import org.bukkit.Bukkit;
import org.bukkit.entity.HumanEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.xm.moretriggercommands.Main;

public class PlayerChangedWorld implements Listener {

    @EventHandler
    public void onPlayerChangedWorld(PlayerChangedWorldEvent e) {
        HumanEntity player = e.getPlayer();
        for (String Command : Main.getInstance().getConfig().getStringList("PlayerChangedWorld.PlayerChangedWorld")) {
            Command = Command
                    .replace("%player_name%", player.getName())
                    .replace("%old_world%", e.getFrom().getName()       )
                    .replace("&", "§");
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), Command);
        }
    }

}