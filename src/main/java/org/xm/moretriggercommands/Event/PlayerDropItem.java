package org.xm.moretriggercommands.Event;


import org.bukkit.Bukkit;
import org.bukkit.entity.HumanEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.xm.moretriggercommands.Main;

public class PlayerDropItem implements Listener {

    @EventHandler
    public void onPlayerDropItem(PlayerDropItemEvent event) {
        HumanEntity player = event.getPlayer();
        for (String Command : Main.getInstance().getConfig().getStringList("PlayerDropItem.PlayerDropItem")) {
            Command = Command
                    .replace("%player_name%", player.getName())
                    .replace("%Drop_name%", event.getItemDrop().getName())
                    .replace("&", "§");
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), Command);
        }
    }

}