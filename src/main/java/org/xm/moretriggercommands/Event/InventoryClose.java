package org.xm.moretriggercommands.Event;


import org.bukkit.Bukkit;
import org.bukkit.entity.HumanEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.xm.moretriggercommands.Main;

public class InventoryClose implements Listener {

    @EventHandler
    public void onInventoryClose(InventoryCloseEvent event) {
        HumanEntity player = event.getPlayer();
        for (String Command : Main.getInstance().getConfig().getStringList("InventoryClose.InventoryClose")) {
            Command = Command.replace("%player_name%", player.getName()).replace("&", "§");
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), Command);
        }
    }

}