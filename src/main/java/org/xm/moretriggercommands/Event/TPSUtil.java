package org.xm.moretriggercommands.Event;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.xm.moretriggercommands.Main;

public class TPSUtil {
    private final Plugin plugin;
    private long lastTickTime;
    private int currentTick;
    private double averageTPS;

    public TPSUtil(Plugin plugin) {
        this.plugin = plugin;
        this.lastTickTime = System.currentTimeMillis();
        this.currentTick = 0;
        this.averageTPS = 20.0;

        Bukkit.getScheduler().runTaskTimer(plugin, this::update, 0L, 1L);
    }

    public void update() {
        int TPStime = Main.getInstance().getConfig().getInt("TPScommand.TPStime");
        int TPS = Main.getInstance().getConfig().getInt("TPScommand.TPSBelow");
        long now = System.currentTimeMillis();
        long tickDuration = now - lastTickTime;
        lastTickTime = now;

        currentTick++;
        if (currentTick % TPStime == 0) { // 多少秒执行
            double instantTPS = 1000.0 / tickDuration;
            String formattedTPS = String.format("%.1f", instantTPS);
                if (instantTPS < TPS) {
                    Bukkit.getScheduler().runTask(plugin, () -> {
                        for (String Command : Main.getInstance().getConfig().getStringList("TPScommand.TPScommand")) {
                            Command = Command.replace("%tps%", formattedTPS).replace("&", "§");
                            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), Command);
                        }
                    });
                }

            averageTPS = (averageTPS * 0.99) + (instantTPS * 0.01);
            currentTick = 0;
        }
    }

    public double getAverageTPS() {
        return averageTPS;
    }
}