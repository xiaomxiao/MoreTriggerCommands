package org.xm.moretriggercommands;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.xm.moretriggercommands.Event.TPSUtil;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class Main extends JavaPlugin implements Listener {
    private static Main instance;

    public static Main getInstance() {
        return instance;
    }
    @Override
    public void onEnable() {

        if (Bukkit.getPluginManager().getPlugin("PlaceholderAPI") != null) {
            Bukkit.getPluginManager().registerEvents(this, this);
        } else {
            throw new RuntimeException("你没有PlaceholderAPI，下载链接https://hangar.papermc.io/HelpChat/PlaceholderAPI");
        }
        instance = this;
        CreateConfig();
        EventSwitch EventSwitch = new EventSwitch();
        EventSwitch.pluginManager();
        getCommand("mtc").setExecutor(new ReloadCommand());
        int pluginId = 17116;
        Metrics metrics = new Metrics(this, pluginId);
        getLogger().info("§aMoreTriggerCommands插件已加载，作者QQ2279881513");
        if (getConfig().getBoolean("update")) {
            try {
                String urlStr = "http://webxm.oss-cn-shenzhen.aliyuncs.com/up.txt?timestamp=" + System.currentTimeMillis();
                URL url = new URL(urlStr); // 指定要抓取数据的网页链接
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setRequestMethod("GET");
                con.setRequestProperty("Cache-Control", "no-cache");

                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuffer content = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    content.append(inputLine);
                }
                in.close();

                String v = content.toString();
                String pv = getDescription().getVersion();
                if (v != pv) {
                    for (int i = 0; i < 5; i++) {
                        System.out.printf("\u001B[32m" + "MoreTriggerCommands已更新至 " + "\u001B[31m" + v + "\u001B[32m" + " 版本请及时更新\u001B[0m\n");
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onDisable() {
        getLogger().info("§aMoreTriggerCommands插件已卸载，作者QQ2279881513");
    }

    public void CreateConfig() {
        File dataFolder = getDataFolder(); // 获取插件文件夹
        File backupConfigFile = new File(dataFolder, "config_backup.yml"); // 定义备份配置文件路径

        // 检查插件文件夹是否存在，如果不存在则创建
        if (!dataFolder.exists()) {
            dataFolder.mkdirs();
            getLogger().info("插件文件夹不存在，已创建");
        }

        // 如果备份文件已存在，先删除
        if (backupConfigFile.exists()) {
            if (backupConfigFile.delete()) {
                getLogger().info("已存在的备份配置文件 config_backup.yml 已删除");
            } else {
                getLogger().severe("无法删除已存在的备份配置文件 config_backup.yml");
                return; // 如果删除失败，直接退出方法
            }
        }

        // 定义需要插入的内容
        String[] insertLines = {
                "# 此文件仅为格式模版，修改无用， 重启重置后会被替换！！！！！！！！！！！！！！",
                "# 此文件仅为格式模版，修改无用， 重启重置后会被替换！！！！！！！！！！！！！！",
                "# 此文件仅为格式模版，修改无用， 重启重置后会被替换！！！！！！！！！！！！！！",
                "# This file is only a format template. Any modifications are useless and will be overwritten after a restart or reset!",
                "# This file is only a format template. Any modifications are useless and will be overwritten after a restart or reset!",
                "# This file is only a format template. Any modifications are useless and will be overwritten after a restart or reset!",
                " ",
                " ",
                " "
        };

        // 从插件资源中获取默认的 config.yml 文件并复制为 config_backup.yml
        try {
            InputStream defaultConfigStream = getResource("config.yml"); // 从插件资源中获取默认的 config.yml
            if (defaultConfigStream != null) {
                // 将插入的内容和默认配置文件内容合并
                StringBuilder contentBuilder = new StringBuilder();
                for (String line : insertLines) {
                    contentBuilder.append(line).append("\n");
                }
                try (BufferedReader reader = new BufferedReader(new InputStreamReader(defaultConfigStream, StandardCharsets.UTF_8))) {
                    String line;
                    while ((line = reader.readLine()) != null) {
                        contentBuilder.append(line).append("\n");
                    }
                }

                // 将合并后的内容写入到 config_backup.yml 文件
                try (FileOutputStream fileOutputStream = new FileOutputStream(backupConfigFile)) {
                    fileOutputStream.write(contentBuilder.toString().getBytes(StandardCharsets.UTF_8));
                }
                getLogger().info("备份配置文件 config_backup.yml 已创建并插入了指定内容");
            } else {
                getLogger().severe("无法从插件资源中获取默认的 config.yml 文件");
            }
        } catch (IOException e) {
            getLogger().severe("创建备份配置文件 config_backup.yml 时发生错误");
            e.printStackTrace();
        }

        // 加载配置文件
        try {
            reloadConfig();
            getLogger().info("插件加载 config 成功");
        } catch (Exception e) {
            e.printStackTrace();
            getLogger().severe("插件无法读取 config");
            getServer().getPluginManager().disablePlugin(this);
        }
    }
}