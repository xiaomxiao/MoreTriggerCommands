package org.xm.moretriggercommands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;

public class ReloadCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (command.getName().equalsIgnoreCase("mtc")) {
            if (args.length == 0) {
                sender.sendMessage("§a[MoreTriggerCommands]:"+"§c/mtc reload 重载配置文件");
                return true;
            }
            if (args[0].equalsIgnoreCase("reload")) {
                if (sender.hasPermission("mtc.reload")) {
                    Main.getInstance().CreateConfig();
                    sender.sendMessage("§a[MoreTriggerCommands]:"+"§c重载成功");
                } else {
                    sender.sendMessage("你都莫得权限，干哈呢？？？？？？？");
                }
                return true;
            }
        }
        return false;
    }
}