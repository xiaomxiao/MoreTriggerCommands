# MoreTriggerCommands

触发事件执行指令
触发事件：自定义无/指令、玩家低于多少血量触发、进服触发、重生触发、死亡触发、资源包触发相关、打开背包触发、关闭背包触发、躺床上触发、离开床触发、对出物品触发、切换世界触发，TPS低于阈值触发、区域触发、击杀玩家触发等，更多正在开发。
预计会把可以做的都写上。

## 安装

1.  把插件扔plugins文件里。修改配置文件config.yml
2.  运行重启服务器。
3.  修改配置文件config.yml后输入`/mtc reload`

## 指令&权限

指令：`mtc reload`：重载插件  
权限：`mtc.reload`：可以使用reload

## 配置
如果有不需要的可以使用[]  
例如:Console-Commands: []
```
# ------------------------------
# 更多功能触发指令插件配置文件
# 修改完成后输入/mtc reload重载此插件（建议重启，部分功能需要重启）
# 建议在测试服中进行测试，防止正式服发生错误
# 更多想法请留言：https://gitee.com/xiaomxiao/MoreTriggerCommands/issues
# 注意：配置文件中大小写敏感，请严格按照要求填写
# 注意：配置文件中大小写敏感，请严格按照要求填写
# 注意：配置文件中大小写敏感，请严格按照要求填写
# 颜色及格式代码示例：
# &0（黑色）、&1（深蓝色）、&2（深绿色）、&3（深青色）、&4（深红色）、&5（深紫色）
# &6（金色）、&7（灰色）、&8（深灰色）、&9（蓝色）、&a（绿色）、&b（青色）、&c（红色）、&d（浅紫色）
# &e（黄色）、&f（白色）、&k（随机字符）、&l（加粗）、&m（删除线）、&n（下划线）、&o（斜体）、&r（重置格式）
# 更多颜色代码请参考：https://minecraft.fandom.com/wiki/Formatting_codes
# ------------------------------

# ------------------------------
# 基础设置
# ------------------------------

# 是否检查更新（true-开启/false-关闭）
update: true

# ------------------------------
# 自定义指令
# ------------------------------

CustomizeCommand:
  # 是否启用自定义指令功能（true-开启/false-关闭）
  Switch: false
  # 自定义指令触发列表
  CCommand:
    - AAA
    - BBB

  # 指令 AAA 配置
AAA:
  # 触发的指令
  Customize-Command:
    - 夜晚到来了
  # 执行的指令（支持变量%player_name%）
  Execute-Command:
    - "say %player_name% ZZZZZ"

# 指令 BBB 配置
BBB:
  # 触发的指令
  Customize-Command:
    - ZZZZZ
  # 执行的指令（支持变量%player_name%）
  Execute-Command:
    - "say 睡觉了，你们干哈呢"

# ------------------------------
# 玩家血量相关
# ------------------------------

# 玩家血量低于指定百分比时执行操作
LifeLessThan:
  # 是否启用该功能（true-开启/false-关闭）
  Switch: false
  # 玩家生命低于多少百分比血量执行
  LifeLessThanPercent: 50 # 百分比
  # 执行的指令
  LifeLessThanCommand:
    - "msg %player_name% &4你的血量低于50%，快嘎了"

# ------------------------------
# 玩家死亡相关
# ------------------------------

# 玩家死亡时执行操作
PlayerDeath:
  # 是否启用该功能（true-开启/false-关闭）
  Switch: false
  # 玩家死亡后多少秒后执行（控制台）/tick(游戏刻) — 每秒钟=20刻
  Console-Command-Cd: 20
  # 由控制台执行的指令，可多行
  Console-Commands:
    - 'say [&a%Deathdate%&f]%player_name%嘎了，第%DeathNumber%次死亡，死亡坐标: %x% %y% %z%  死亡世界： %world% 死亡原因:%DeathMsg%'
  # 玩家死亡后多少秒后执行（玩家）/tick(游戏刻) — 每秒钟=20刻
  Player-Command-Cd: 10
  # 由玩家执行的指令，可多行
  Player-Commands:
    - 'msg %player_name% [&a%Deathdate%&f]你嘎了%DeathNumber%次了，你的死亡坐标 : %x% %y% %z%  死亡世界： %world% 死亡原因:%DeathMsg%'
  # 时间格式
  Date: yyyy-MM-dd HH:mm:ss
  # 可用的变量：
  # 玩家名：%player_name%，
  # X轴：%x%，
  # Y轴：%y%，
  # Z轴：%z%，
  # 世界：%world%，
  # 死亡原因：%DeathMsg%，
  # 玩家死亡次数：%DeathNumber%，
  # 玩家死亡时间（系统时间）：%Deathdate%。

# 玩家重生时执行操作
PlayerRespawn:
  # 是否启用该功能（true-开启/false-关闭）
  Switch: false
  # 玩家重生后多少秒后执行（控制台）/tick(游戏刻) — 每秒钟=20刻
  Console-Command-Cd: 20
  # 由控制台执行的指令，可多行
  Console-Commands:
    - 'say [&a%Deathdate%&f]%player_name%重生了，第%DeathNumber%次死亡，死亡世界： %world% 重生的位置:%RespawnLocation%'
  # 玩家重生后多少秒后执行（玩家）/tick(游戏刻) — 每秒钟=20刻
  Player-Command-Cd: 10
  # 由玩家执行的指令，可多行
  Player-Commands:
    - 'msg %player_name% [&a%Deathdate%&f]%player_name%重生了，第%DeathNumber%次死亡，死亡世界： %world% 重生的位置:%RespawnLocation%'
  # 时间格式
  Date: yyyy-MM-dd HH:mm:ss
  # 可用的变量：
  # 玩家名：%player_name%，
  # 世界：%world%，
  # 玩家死亡次数：%DeathNumber%，
  # 重生的位置：%RespawnLocation%，
  # 玩家死亡时间（系统时间）：%Deathdate%。

# 玩家杀死其他玩家时执行操作
KillNotifier:
  # 是否启用该功能（true-开启/false-关闭）
  Switch: false
  # 执行的指令
  KillNotifierCommand:
    - "say &1%player_name% &c杀死了 &1%Drop_name%"
    # 可用的变量：
    # 杀手：%player_name%，
    # 被杀：%Drop_name%，

# ------------------------------
# 玩家进服相关
# ------------------------------

Join:
  # 是否启用进服相关功能（true-开启/false-关闭）
  Switch: false
  # 首次进服执行指令
  FirstCommand:
    - "msg %player_name% &a第一次进服"
  # 每次进服执行的指令
  EnterCommands:
    - "msg %player_name% %player_name%：&a爷进服了"

# ------------------------------
# 资源包相关
# ------------------------------

ResourcePackStatus:
  # 是否启用资源包状态检测功能（true-开启/false-关闭）
  Switch: false
  # 资源包成功地下载并应用到了客户端
  SUCCESSFULLY_LOADEDCommands:
    - "msg %player_name% %player_name%：资源包成功地下载并应用到了客户端"
  # 客户端接受了资源包，但下载失败
  FAILED_DOWNLOADCommands:
    - "msg %player_name% %player_name%：客户端接受了资源包，但下载失败。"
  # 客户端拒绝接受资源包
  DECLINEDCommands:
    - "msg %player_name% %player_name%：客户端拒绝接受资源包。"
  # 客户端接受了资源包，并开始下载
  ACCEPTEDCommands:
    - "msg %player_name% %player_name%：客户端接受了资源包，并开始下载。"

# ------------------------------
# 物品操作相关
# ------------------------------

# 玩家打开背包（容器，E键打开无用）后执行
InventoryOpen:
  # 是否启用该功能（true-开启/false-关闭）
  Switch: false
  InventoryOpen:
    - "msg %player_name% 你打开了容器"

# 玩家关闭背包后执行
InventoryClose:
  # 是否启用该功能（true-开启/false-关闭）
  Switch: false
  InventoryClose:
    - "msg %player_name% 你关闭了背包"

# 玩家丢出物品时触发（%Drop_name%获取玩家丢出的物品）
PlayerDropItem:
  # 是否启用该功能（true-开启/false-关闭）
  Switch: false
  PlayerDropItem:
    - "msg %player_name% 你丢了个%Drop_name%"

# ------------------------------
# 床相关
# ------------------------------

# 玩家躺到床上时执行（白天依然有效）
PlayerBedEnter:
  # 是否启用该功能（true-开启/false-关闭）
  Switch: false
  PlayerBedEnter:
    - "msg %player_name% 你躺上了床"

# 玩家离开床时执行
PlayerBedLeave:
  # 是否启用该功能（true-开启/false-关闭）
  Switch: false
  PlayerBedLeave:
    - "msg %player_name% 你离开了床"

# ------------------------------
# 服务器性能相关
# ------------------------------

# TPS低于指定值时触发操作
TPScommand:
  # 是否启用该功能（true-开启/false-关闭）
  Switch: false
  # 多少秒判断一次TPS /tick(游戏刻) — 每秒钟=20刻
  TPStime: 20
  # 当TPS低于该值时触发
  TPSBelow: 10
  # 执行的指令
  TPScommand:
    - "say &c干JB啥呢？？？，搞的服务器这么卡！,挖槽现在tps &l%tps%"

# ------------------------------
# 世界相关
# ------------------------------

# 玩家切换到另一个世界时触发
PlayerChangedWorld:
  # 是否启用该功能（true-开启/false-关闭）
  Switch: false
  # 执行的指令
  PlayerChangedWorld:
    - "msg %player_name% 你从%old_world%传送到了这里"

# ------------------------------
# 区域进入检测系统
# ------------------------------

JoinRegion:
  # 是否启用区域检测功能（true-启用/false-禁用）
  Switch: false
  # 需要注册的区域ID列表（对应下方区域配置）
  Region:
    - 1Q  # 末地保护区配置
    - 2Q  # 地狱保护区配置

# 区域配置示例（可复制多个）
# 配置ID需要与上方JoinRegion.Region列表对应
1Q:
  # 区域显示名称（支持中文，用于消息变量）
  RegionName: "末地保护区"
  # 区域所在世界名称（必须与服务器世界名称完全一致）
  RegionWorld: world_the_end
  # 区域范围配置（使用XZ坐标，Y轴全高度）
  # 两个对角点坐标（自动计算最大最小范围）
  corner1:
    x: 100  # 第一个角点X坐标
    z: 100  # 第一个角点Z坐标
  corner2:
    x: 200  # 第二个角点X坐标
    z: 200  # 第二个角点Z坐标
  # 进入区域时执行的指令列表（按顺序执行）
  Join-Command:
    - "msg %player_name% 你进入了%region_name%"  # 发送提示消息
  # 离开区域时执行的指令列表
  Exit-Command:
    - "msg %player_name% 你走出了%region_name%"

2Q:
  # 区域显示名称（支持中文，用于消息变量）
  RegionName: "地狱危险区"
  # 区域所在世界名称（必须与服务器世界名称完全一致）
  RegionWorld: world_nether
  # 区域范围配置（使用XZ坐标，Y轴全高度）
  # 两个对角点坐标（自动计算最大最小范围）
  corner1:
    x: 50  # 第一个角点X坐标
    z: 50  # 第一个角点Z坐标
  corner2:
    x: 90  # 第二个角点X坐标
    z: 90  # 第二个角点Z坐标
  # 进入区域时执行的指令列表（按顺序执行）
  Join-Command:
    - "msg %player_name% 你进入了%region_name%"  # 发送提示消息
  # 离开区域时执行的指令列表
  Exit-Command:
    - "msg %player_name% 你走出了%region_name%"
```


## 更新日志

### v1.1.3
- **新功能**
  - 区域指令执行功能（[#IBJZY4](https://gitee.com/xiaomxiao/MoreTriggerCommands/issues/IBJZY4)）
    - 新增进入和退出区域时执行指令的功能。管理员可在配置文件中定义区域的范围（世界、坐标）和进入/退出时的指令。
    - 玩家进入或离开区域时，系统自动执行对应指令，可用于触发欢迎消息、任务奖励或权限变更等操作。
    - 区域支持玩家移动、传送和加入游戏时的动态检测。
  - 击杀玩家执行指令
    - 新增击杀玩家时执行指令的功能。当玩家被击杀时，系统会根据配置执行相关指令，可用于记录击杀信息或触发奖励机制。
- **修复**
  - 修复部分已知问题。

### v1.1.2
- **新功能**
  - 添加TPS触发功能，当TPS低于阈值时触发事件。
  - 新增变量`%TPS%`，可在指令中使用。
  - 添加`&`颜色字符支持。

### v1.1.1
- **新功能**
  - 添加自定义无/指令（[#I6YE6X](https://gitee.com/xiaomxiao/MoreTriggerCommands/issues/I6YE6X)）

### v1.0
- **初始发布**
  - 插件首次发布。





![bstats](https://bstats.org/signatures/bukkit/MoreTriggerCommands.svg "插件信息图")